CXX := g++
INC := -I./include/ -I./include/GL
CXXFLAGS = -std=c++11 -Wall -Wextra -Werror -pedantic -save-temps=obj $(INC)
DFLAGS := -g

LIB = libGL++.a

SRC := $(wildcard src/*.cpp)
DOBJS := $(SRC:src/%.cpp=obj/Debug/%.o)
ROBJS := $(SRC:src/%.cpp=obj/Release/%.o)
DEPS := $(SRC:%.cpp=.deps/Debug/%.cpp.d) $(SRC:%.cpp=.deps/Release/%.cpp.d)

.PHONY: all
all: setup debug release

-include $(DEPS)

debug: bin/Debug/$(LIB)
release: bin/Release/$(LIB)

bin/Debug/$(LIB): $(DOBJS) obj/Debug/glew.o
	ar rcs $@ $^

bin/Release/$(LIB): $(ROBJS) obj/Release/glew.o
	ar rcs $@ $^

#Compile the file and write its dependancies for the next time make is invoked
obj/Debug/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/Debug/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/Release/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $(RFLAGS) -MM -MF .deps/Release/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

#No dependency for glew.c, but this file will not change, so it's not needed
obj/Debug/glew.o: src/glew.c
	gcc $(INC) -c $< -o $@

obj/Release/glew.o: src/glew.c
	gcc $(INC) -c $< -o $@

setup:
	mkdir -p obj/Debug obj/Release bin/Debug bin/Release .deps/Debug/src .deps/Release/src 2> /dev/null

.PHONY: clean

clean:
	rm -rf $(DOBJS) $(ROBJS) $(DEPS) bin/Debug/$(LIB) bin/Release/$(LIB)
