#ifndef GL_WINDOW
#define GL_WINDOW

#include <string>
#include <SDL2/SDL_opengl.h>

/*! The main namespace */
namespace GL {

    /*! A SDL2 Window automatically created and freed that can be rendered into. It is double-buffered. */
    class Window {
    public:
	/*! Constructs a Window from the full set of parameters
	  \param[in] name The title of the window
	  \param[in] w The width of the window
	  \param[in] h The height of the window
	  \param[in] x The x coordinate of the window.
	  \param[in] y The y coordinate of the window.
	  \param[in] flags Flags passed to the SDL2 window.
	  \param[in] glMajVer The OpenGL major version (1 to 4)
	  \param[in] glMinVer The OpenGL minor version
	 */
        explicit Window(std::string const& name, int w, int h, int x = SDL_WINDOWPOS_CENTERED, int y = SDL_WINDOWPOS_CENTERED, int flags = 0, int glMajVer = 3, int glMinVer = 3);
	Window(Window const&) = delete;
	Window& operator=(Window const&) = delete;
	Window(Window&&) = delete;
	Window& operator=(Window&&) = delete;
	/*! Free resources associated to the Window */
	~Window();

	/*! Render the OpenGL drawing operations, swapping buffers. */
	void Render(void) const noexcept;
	
    private:
	SDL_Window* window{};
	SDL_GLContext context{};
    };
}

#endif //GL_WINDOW
