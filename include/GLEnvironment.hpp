#ifndef GLENVIRONMENT_HPP
#define GLENVIRONMENT_HPP

/// The main namespace
namespace GL {

    //The only purpose of its class is its constructor, for now: it will initialise the environment for OpenGL and Glew
    //It should be called before doing any OpenGL activity.
    //Maybe at a later time, the destructor will be useful for cleaning up.

    /*! RAII-like wrapper for the whole OpenGL environment */
    class Environment {
    public:
	Environment();
	~Environment();

    private:
	static const int depthSize{24};
    };
}

#endif //GLENVIRONMENT_HPP
