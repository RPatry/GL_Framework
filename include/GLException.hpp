#ifndef GLEXCEPTION_HPP
#define GLEXCEPTION_HPP

#include <string>
#include <exception>

/*! \file */

/// The main namespace
namespace GL {

    /*! The base exception for all the exceptions found in the framework. It can be used by itself too.
     */
    struct GLException : std::exception {
	/*! Construct the exception from an error message. */
	GLException(std::string const& message) :
	    message{message}
	{  }
	/*! Construct the exception from an error message. */
	GLException(const char* message) :
	    message{message}
	{  }
	/*! Returns a message providing context about the error. */
	const char* what(void) const noexcept override {
	    return message.c_str();
	}
    private:
	const std::string message{};
    };
}

#endif //GLEXCEPTION_HPP
