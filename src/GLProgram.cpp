#include "glew.h"
#include <algorithm>
#include <utility>
#include <sstream>
#include "GLProgram.hpp"
#include "GLShader.hpp"

using namespace std;

namespace GL {

    Program::Program(GLuint program) {
        if (!IsProgram(program)) {
	    throw ProgramLoadingException{"Error: the specified program name doesn't actually refer to a valid program!"};
	}
	this->program = program;
    }

    Program::Program(void) :
	program{ glCreateProgram() }
    {
    }

    Program::Program(vector<Shader> const& shaders) :
	Program{}
    {
	program = glCreateProgram();
	AttachShaders(shaders);
    }

    Program::Program(Program const& program) :
	Program{}  //Init this->program
    {
	//We first need to make a copy of the associated shaders.
	auto assocShaders = ProgramAssociatedShaders(program.program);
	vector<Shader> shaders{};
	for (GLuint s : assocShaders) {
	    shaders.push_back(CopyShaderFromName(s));
	}
	AttachShaders(shaders);
	//Also copy linking status
	if (ProgramLinkSuccess(program.program)) {
	    Link();
	}
	//And here the shaders get flagged for deletion.
    }

    Program& Program::operator=(Program const& program) {
	Program copy{program};
	swap(*this, copy);
	return *this;
    }

    Program::Program(Program&& program) {
	this->program = program.program;
	program.program = 0;
    }

    Program& Program::operator=(Program&& program) {
	swap(this->program, program.program);
	return *this;
    }

    Program::~Program() {
	glDeleteProgram(program);
    }

    void Program::AttachShaders(vector<Shader> const& shaders) noexcept {
	auto assocShaders = GetAssociatedShaders();
	for (Shader const& s : shaders) {
	    if (find(assocShaders.begin(), assocShaders.end(), s.GetName()) == assocShaders.end()) {
		glAttachShader(program, s);  //Don't attach a program twice.
	    }
	}
    }

    void Program::Link(bool detach) {
	glLinkProgram(program);
	if (!ProgramLinkSuccess(program)) {
	    const string log { GetInfoLog() };
	    stringstream ss;
	    ss << "Failed to link the program!\nExtra information: " << log;
	    throw ProgramLinkingException{ss.str()};
	}
	if (detach) {
	    auto assoc = GetAssociatedShaders();
	    for (GLuint shader : assoc) {
		glDetachShader(program, shader);
	    }
	}
    }

    void Program::DetachShaders(std::vector<Shader> const& shaders) noexcept {
	auto assoc = GetAssociatedShaders();
	for (Shader const& shader : shaders) {
	    if (find(assoc.begin(), assoc.end(), shader.GetName()) != assoc.end()) {
		glDetachShader(program, shader);
	    }
	}
    }

    Program::operator GLuint(void) const noexcept {
	return program;
    }

    vector<GLuint> Program::GetAssociatedShaders(void) const noexcept {
	return ProgramAssociatedShaders(program);
    }

    vector<GLuint> Program::ProgramAssociatedShaders(GLuint program) {
 	vector<GLuint> shaders;
	GLint nbShaders{};
	//Fetch the numbers of associated shaders.
	glGetProgramiv(program, GL_ATTACHED_SHADERS, &nbShaders);
	shaders.resize(static_cast<size_t>(nbShaders));
	//Fetch the shaders themselves.
	glGetAttachedShaders(program, nbShaders, nullptr, &shaders[0]);

	return shaders;
    }

    string Program::GetInfoLog(void) const noexcept {
	GLint lengthLog;
	string log;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &lengthLog);
	log.resize(static_cast<size_t>(lengthLog));
	glGetProgramInfoLog(program, lengthLog, nullptr, &log[0]);
	return log;
    }
}
